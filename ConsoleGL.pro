TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += c++11


SOURCES += main.cpp \
    consolewindow.cpp \
    matrix.cpp \
    model.cpp \
    camera.cpp

HEADERS += \
    consolewindow.h \
    types.h \
    matrix.h \
    model.h \
    utils.h \
    camera.h


unix:!macx: LIBS += -lncurses
