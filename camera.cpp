#include "camera.h"

const Matrix4x4 &Camera::GetViewMatrix() const {
  if (!is_view_matrix_correct_) {
    ComputeViewMatrix();
  }

  return view_matrix_;
}

void Camera::ComputeViewMatrix() const {
  Matrix3x3 basis_matrix;
  for (int row = 0; row < 3; ++row) {
    basis_matrix[row][0] = right_[row];
    basis_matrix[row][1] = up_[row];
    basis_matrix[row][2] = view_direction_[row];
  }

  Matrix3x3 inverse_basis_matrix = Inverse(basis_matrix);

  for (int row = 0; row < 3; ++row) {
    for (int column = 0; column < 3; ++column) {
      view_matrix_[row][column] = inverse_basis_matrix[row][column];
    }
    view_matrix_[row][3] = -position_[row];
  }

  for (int column = 0; column < 3; ++column) {
    view_matrix_[3][column] = 0;
  }
  view_matrix_[3][3] = 1;
  is_view_matrix_correct_ = true;
}

