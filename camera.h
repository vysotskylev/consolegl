#ifndef CAMERA_H
#define CAMERA_H

#include <matrix.h>

class Camera
{
  public:
    Camera(Vector3 position, Vector3 view_direction, Vector3 up, Vector3 right,
           float horizontal_view_angle, float vertical_view_angle)
        : position_(position), view_direction_(view_direction),
          up_(up), right_(right),
          horizontal_view_angle_(horizontal_view_angle),
          vertical_view_angle_(vertical_view_angle) {
      ComputeViewMatrix();
    }

    const Matrix4x4 &GetViewMatrix() const;

    float horizontal_view_angle() const {
      return horizontal_view_angle_;
    }

    float vertical_view_angle() const {
      return vertical_view_angle_;
    }

  private:
    Vector3 position_;
    Vector3 view_direction_;
    Vector3 up_, right_;

    float horizontal_view_angle_;
    float vertical_view_angle_;

    mutable Matrix4x4 view_matrix_;
    mutable bool is_view_matrix_correct_;

    void ComputeViewMatrix() const;
};

#endif // CAMERA_H
