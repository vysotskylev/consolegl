#include <consolewindow.h>
#include <curses.h>




ConsoleWindow::ConsoleWindow()
{
  window_ptr_ = initscr();
  int rows, columns;
  getmaxyx(window_ptr_, rows, columns);

  buffer_ = WindowBuffer(rows, columns, ' ');
}

void ConsoleWindow::SetPixel(int row, int column, Color color)
{
  buffer_.at(row, column) = color;
}

void ConsoleWindow::SetBuffer(const WindowBuffer &buffer)
{
  buffer_ = buffer;
}

void ConsoleWindow::Fill(Color color)
{
  for (int row = 0; row < buffer_.height(); ++row) {
    for (int column = 0; column < buffer_.width(); ++column) {
      buffer_.at(row, column) = color;
    }
  }
}

void ConsoleWindow::Draw()
{
  clear();
  for (int row = 0; row < buffer_.height(); ++row) {
    for (int column = 0; column < buffer_.width(); ++column) {
      addch(buffer_.at(row, column));
    }
  }
  refresh();
}
