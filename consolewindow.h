#ifndef CONSOLEWINDOW_H
#define CONSOLEWINDOW_H

#include <types.h>

#include <curses.h>

class ConsoleWindow
{
  public:
    ConsoleWindow();

    void SetPixel(int row, int column, Color color);

    void SetBuffer(const WindowBuffer &buffer);

    void Fill(Color color);

    void Draw();

    inline int width() const {
      return buffer_.width();
    }

    inline int height() const {
      return buffer_.height();
    }

    ~ConsoleWindow() {
      endwin();
    }

  private:
    WindowBuffer buffer_;
    WINDOW *window_ptr_;
};

#endif // CONSOLEWINDOW_H
