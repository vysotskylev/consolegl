#include <iostream>
#include <chrono>
#include <vector>
#include <cmath>

#include "types.h"
#include "consolewindow.h"
#include "model.h"
#include "matrix.h"
#include "camera.h"

std::ostream &operator<<(std::ostream &os, const Vector3 &v) {
  return os << v[0] << ' ' << v[1] << ' ' << v[2];
}

std::ostream &operator<<(std::ostream &os, const Matrix3x3 &mat) {
  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 3; ++j) {
      os << mat[i][j] << ' ';
    }
    os << '\n';
  }
  return os;
}




void TestMatrixOperations() {
  using std::cout;
  using std::endl;

  Vector3 right = {1,2,3};
  float arr[3][3] = {{3,5,7}, {-1,3,-7}, {3,1,9}};
  Matrix3x3 mat;
  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 3; ++j) {
      mat[i][j] = arr[i][j];
    }
  }

  Matrix3x3 inverse = Inverse(mat);
  Vector3 answer = inverse * right;

  cout << "inverse: \n";
  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 3; ++j) {
      cout << inverse[i][j] << ' ';
    }
    cout << '\n';
  }
  //Solution solution = Solve(mat, right);
  cout << "solution = " << answer << endl;

  Vector3 new_right = mat * answer;
  cout << "new_right = " << new_right << endl;
}

Vector4 ToUniform(const Vector3 &vector3) {
  Vector4 result;
  for (int i = 0; i < 3; ++i) {
    result[i] = vector3[i];
  }
  result[3] = 1.0;

  return result;
}

void FillTriangleWithHorizontalBase(
    Vector2int base_left, Vector2int base_right, Vector2int third, WindowBuffer &buffer) {

}

void FillTriangle(const std::vector<Vector2int> &screen_points, WindowBuffer &buffer) {
  std::sort(screen_points.begin(), screen_points.end(),
            [](const Vector2int &lhs, const Vector2int &rhs) {
              return lhs[0] < rhs[0];
            });

  Vector2int high = screen_points[0];
  Vector2int middle = screen_points[1];
  Vector2int low = screen_points[2];

  int cutting_row = middle[0];
  if (high[0] == low[0]) {
    // Degenerate triangle
    return;
  }

  float cutting_ratio = float(cutting_row - low[0]) / (high[0] - low[0]);
  int cutting_column = low[1] + (high[1] - low[1]) * cutting_ratio;

  Vector2int middle_left = {cutting_row, std::min(middle[1], cutting_column)};
  Vector2int middle_right = {cutting_row, std::max(middle[1], cutting_column)};

  FillTriangleWithHorizontalBase(middle_left, middle_right, low, buffer);
  FillTriangleWithHorizontalBase(middle_left, middle_right, high, buffer);
}

void RenderModel(const Model &model, const Camera &camera, WindowBuffer &buffer) {

  float screen_width = 2 * std::tan(camera.horizontal_view_angle() / 2);
  float screen_height = 2 * std::tan(camera.vertical_view_angle() / 2);

  int screen_width_pixels = buffer.width();
  int screen_height_pixels = buffer.height();

  float pixel_width = screen_width / screen_width_pixels;
  float pixel_height = screen_height / screen_height_pixels;

  Matrix4x4 view_matrix = camera.GetViewMatrix();

  Matrix4x4 projection_matrix;
  for (int i = 0; i < 3; ++i) {
    projection_matrix[i][i] = 1;
  }
  projection_matrix[3][2] = 1;

  Matrix4x4 total_matrix = projection_matrix * view_matrix;

  for (const auto &triangle: model.GetTriangles()) {
    std::vector<Vector2int> screen_points(3);
    bool any_in_screen = false;

    for (int point_in_triangle_index = 0;
         point_in_triangle_index < 3;
         ++point_in_triangle_index) {

      Vector4 point = ToUniform(model.GetPoint(triangle[point_in_triangle_index]));
      Vector4 projected = total_matrix * point;

      float x = projected[0] / projected[3];
      float y = projected[1] / projected[3];


      int x_pixels = x / pixel_width;
      int y_pixels = y / pixel_height;

      int row = screen_height_pixels / 2 - y_pixels;
      int column = screen_width_pixels / 2 + x_pixels;

      screen_points[point_in_triangle_index] = Vector2int{row, column};
      if (row >= 0 && row < buffer.height() && column >= 0 && column < buffer.width()) {
        any_in_screen = true;
      }
    }

    if (any_in_screen) {
      FillTriangle(screen_points, buffer);
    }
  }
}

void Render(const std::vector<Model> &models, const Camera &camera, WindowBuffer &buffer) {
  for (const auto &model : models) {
    RenderModel(model, camera, buffer);
  }
}


void Wait(int millisec_to_wait){
  using clock = std::chrono::high_resolution_clock;

  auto start = clock::now();
  while (true) {
    auto now = clock::now();
    auto passed = std::chrono::duration_cast<std::chrono::milliseconds>(now - start).count();
    if (passed >= millisec_to_wait) {
      return;
    }
  }
}

int main()
{
  //TestMatrixOperations();
  //return 0;
    ConsoleWindow window;
    WindowBuffer buffer(window.height(), window.width(), ' ');


    Vector3 points[3] = {{10, 0, 3}, {10, 0, -3}, {10,3,0}};
    Vector3 normals[3];
    Triangle triangles[1] = {{0,1,2}};

    Model model(points, points + 3, normals, normals + 3, triangles, triangles + 1);

    Camera camera({0,0,0}, {1,0,0}, {0,1,0}, {0,0,1}, 2, 2);


  int i = 0;

  for (i = 0; i < 200; ++i) {
    buffer.Fill(' ');
    model.Move({-0.1, 0,0});
    RenderModel(model, camera, buffer);
    window.SetBuffer(buffer);

    window.Draw();

    Wait(30);
    //window.SetPixel(5,i % window.width(), 'x');
  }

  int x;
  std::cin >> x;
  return 0;
}

