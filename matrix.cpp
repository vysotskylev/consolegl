#include "matrix.h"
#include <cmath>



inline void SwapRows(Matrix3x3 &matrix, int first, int second) {
  for (int i = 0; i < 3; ++i) {
    std::swap(matrix[first][i], matrix[second][i]);
  }
}

//Solution Solve(Matrix3x3 matrix, Vector3 right) {
//  const int rows = 3;
//  const int columns = 3;

//  for (int i = 0; i < rows; ++i) {
//    int nonzero_row = i;
//    while (nonzero_row < rows && FloatEqual(matrix[nonzero_row][i], 0.0)) {
//      nonzero_row++;
//    }
//    if (nonzero_row == rows) {
//      return {false};
//    }

//    SwapRows(matrix, i, nonzero_row);
//    std::swap(right[i], right[nonzero_row]);

//    for (int other_row = i + 1; other_row < rows; ++other_row) {
//      float multiple = matrix[other_row][i] / matrix[i][i];
//      for (int j = i; j < columns; ++j) {
//        matrix[other_row][j] -= matrix[i][j] * multiple;
//      }
//      right[other_row]     -= right[i]     * multiple;
//    }
//  }

//  Vector3 result;
//  for (int i = rows - 1; i >= 0; --i) {
//    result[i] = right[i] / matrix[i][i];
//    for (int other_row = i - 1; other_row >= 0; --other_row) {
//      right[other_row] -= matrix[other_row][i] / matrix[i][i] * right[i];
//    }
//  }

//  return {true, result};
//}


//Vector3 Multiply(const Matrix3x3 &matrix, const Vector3 &vec) {
//  Vector3 result;
//  for (int i = 0; i < 3; ++i) {
//    result[i] = 0.0;
//    for (int j = 0; j < 3; ++j) {
//      result[i] += matrix[i][j] * vec[j];
//    }
//  }

//  return result;
//}
