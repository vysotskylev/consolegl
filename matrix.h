#ifndef MATRIX_H
#define MATRIX_H

#include <vector>
#include <initializer_list>
#include <cassert>
#include <array>
#include <cmath>

template<typename T>
class Matrix
{
  public:
    Matrix(int rows = 0, int columns = 0, const T &init = T())
        : matrix_(rows, std::vector<T>(columns, init)) {
    }

    const T &at(int row, int column) const {
      return matrix_[row][column];
    }

    T &at(int row, int column) {
      return matrix_[row][column];
    }

    int width() const {
      if (matrix_.empty()) {
        return 0;
      }
      return matrix_[0].size();
    }

    int height() const {
      return matrix_.size();
    }

    void Fill(const T &val) {
      for (auto &row : matrix_) {
        row.assign(row.size(), val);
      }
    }

  private:
    std::vector<std::vector<T>> matrix_;
};








template<typename T, int kNumRows, int kNumColumns>
class FixedMatrix {
  public:
    FixedMatrix(const T &init = T()) {
      for (int i = 0; i < kNumRows; ++i) {
        for (int j = 0; j < kNumColumns; ++j) {
          mat_[i][j] = init;
        }
      }
    }

    std::array<T, kNumColumns> &operator[] (int i) {
      return mat_[i];
    }

    const std::array<T, kNumColumns> &operator[] (int i) const {
      return mat_[i];
    }

  private:
    std::array<std::array<T, kNumColumns>, kNumRows> mat_;
};

template<typename T, int kSize>
class FixedVector {
  public:
    FixedVector(std::initializer_list<T> list) {
      assert(list.size() == kSize);
      auto iter = list.begin();
      for (int i = 0; i < kSize; ++i) {
        vec_[i] = *iter++;
      }
    }

    FixedVector(const T &init = T()) {
      for (int i = 0; i < kSize; ++i) {
        vec_[i] = init;
      }
    }

    T &operator[] (int i) {
      return vec_[i];
    }

    const T &operator[] (int i) const {
      return vec_[i];
    }

    FixedVector &operator+=(const FixedVector &other) {
      for (int i = 0; i < kSize; ++i) {
        vec_[i] += other[i];
      }
      return *this;
    }

  private:
    std::array<T, kSize> vec_;
};



using Matrix3x3 = FixedMatrix<float, 3, 3>;
using Matrix4x4 = FixedMatrix<float, 4, 4>;
using Vector3   = FixedVector<float, 3>;
using Vector4   = FixedVector<float, 4>;
using Vector2int  = FixedVector<int, 2>;


inline bool FloatEqual(float a, float b) {
  return std::fabs(a - b) < 0.0000001;
}


template<typename T, int kRowsFirst, int kColumnsFirst, int kColumnsSecond>
FixedMatrix<T, kRowsFirst, kColumnsSecond>
operator* (FixedMatrix<T, kRowsFirst, kColumnsFirst> &first,
           FixedMatrix<T, kColumnsFirst, kColumnsSecond> &second) {
  FixedMatrix<T, kRowsFirst, kColumnsSecond> result;

  for (int i = 0; i < kRowsFirst; ++i) {
    for (int j = 0; j < kColumnsSecond; ++j) {
      for (int k = 0; k < kColumnsFirst; ++k) {
        result[i][j] += first[i][k] * second[k][j];
      }
    }
  }

  return result;
}

template<typename T, int kRows, int kColumns>
FixedVector<T, kRows>
operator* (FixedMatrix<T, kRows, kColumns> &matrix,
           FixedVector<T, kColumns> &vector) {
  FixedVector<T, kRows> result;

  for (int i = 0; i < kRows; ++i) {
    for (int j = 0; j < kColumns; ++j) {
      result[i] += matrix[i][j] * vector[j];
    }
  }

  return result;
}






template<typename T, int kRows, int kColumns>
void MultiplyRow(FixedMatrix<T, kRows, kColumns> &matrix,  int row, const T &val) {
  for (int column = 0; column < kColumns; ++column) {
    matrix[row][column] *= val;
  }
}

template<typename T, int kRows, int kColumns>
void SubtractRowWithMultiplication(FixedMatrix<T, kRows, kColumns> &matrix,
                                   int first_row, int second_row, const T &multiple) {
  for (int column = 0; column < kColumns; ++column) {
    matrix[first_row][column] -= matrix[second_row][column] * multiple;
  }
}

template<int kSize>
FixedMatrix<float, kSize, kSize> Inverse(FixedMatrix<float, kSize, kSize> matrix) {
  FixedMatrix<float, kSize, kSize> result;
  for (int i = 0; i < kSize; ++i) {
    result[i][i] = 1;
  }

  for (int i = 0; i < kSize; ++i) {
    int nonzero_row = i;
    while (nonzero_row < kSize && FloatEqual(matrix[nonzero_row][i], 0.0)) {
      nonzero_row++;
    }
    assert(nonzero_row != kSize);

    std::swap(matrix[i], matrix[nonzero_row]);
    std::swap(result[i], result[nonzero_row]);

    float multiple = 1.0 / matrix[i][i];
    MultiplyRow(matrix, i, multiple);
    MultiplyRow(result, i, multiple);

    for (int other_row = i + 1; other_row < kSize; ++other_row) {
      float multiple = matrix[other_row][i];
      SubtractRowWithMultiplication(matrix, other_row, i, multiple);
      SubtractRowWithMultiplication(result, other_row, i, multiple);
    }
  }

  for (int i = kSize - 1; i >= 0; --i) {
    for (int other_row = i - 1; other_row >= 0; --other_row) {
      float multiple = matrix[other_row][i];
      SubtractRowWithMultiplication(result, other_row, i, multiple);
    }
  }

  return result;
}



template<typename T, int kSize>
inline T Dot(const FixedVector<T, kSize> &a, const FixedVector<T, kSize> &b) {
  T result;
  for (int i = 0; i < kSize; ++i) {
    result += a[i] * b[i];
  }
}


template<typename T, int kSize>
inline FixedVector<T, kSize> operator-(const FixedVector<T, kSize> &a,
                                       const FixedVector<T, kSize> &b) {
  FixedVector<T, kSize> result;
  for (int i = 0; i < kSize; ++i) {
    result[i] = a[i] - b[i];
  }
  return result;
}



#endif // MATRIX_H
