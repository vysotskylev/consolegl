#ifndef MODEL_H
#define MODEL_H

#include <vector>

#include "types.h"
#include "utils.h"

class Model
{
  public:
    using ConstPointIterator = std::vector<Vector3>::const_iterator;
    using ConstNormalIterator = std::vector<Vector3>::const_iterator;
    using ConstTriangleIterator = std::vector<Triangle>::const_iterator;

    template<typename PointIt, typename NormalIt, typename TriangleIt>
    Model(PointIt first_point, PointIt last_point,
          NormalIt first_normal, NormalIt last_normal,
          TriangleIt first_triangle, TriangleIt last_triangle);

    IteratorRange<ConstPointIterator> GetPoints() const {
      return {points_.begin(), points_.end()};
    }

    Vector3 GetPoint(int i) const {
      return points_[i];
    }

    Vector3 GetNormal(int i) const {
      return normals_[i];
    }

    IteratorRange<ConstNormalIterator> GetNormals() const {
      return {normals_.begin(), normals_.end()};
    }


    IteratorRange<ConstTriangleIterator> GetTriangles() const {
      return {triangles_.begin(), triangles_.end()};
    }

    void Move(Vector3 delta);

  protected:
    std::vector<Vector3> points_;
    std::vector<Vector3> normals_;
    std::vector<Triangle> triangles_;
};


template<typename PointIt, typename NormalIt, typename TriangleIt>
Model::Model(PointIt first_point, PointIt last_point,
      NormalIt first_normal, NormalIt last_normal,
      TriangleIt first_triangle, TriangleIt last_triangle)
  : points_(first_point, last_point),
    normals_(first_normal, last_normal),
    triangles_(first_triangle, last_triangle) {

}

#endif // MODEL_H
