#ifndef TYPES_H
#define TYPES_H

#include <cstdint>
#include <matrix.h>


using Color = uint8_t;

using WindowBuffer = Matrix<Color>;


struct Triangle {
  int points[3];
};



#endif // TYPES_H
