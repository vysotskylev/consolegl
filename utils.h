#ifndef UTILS_H
#define UTILS_H

template<typename Iterator>
class IteratorRange {
  public:
    IteratorRange(Iterator begin, Iterator end)
        : begin_(begin), end_(end) {

    }

    Iterator begin() {
      return begin_;
    }

    Iterator end() {
      return end_;
    }

  private:
    Iterator begin_, end_;
};

#endif // UTILS_H
